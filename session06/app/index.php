<?php
//echo 'pages/' . $_GET['p'] . '.php';
//require_once 'pages/' . $_GET['page'] . '.php';

require __DIR__ . '/vendor/autoload.php';
session_start();
require_once 'app/config/config.php';
require_once 'app/libs/auth.php';


require_once 'app/template/header.php';
switch ($_GET['p']) {
    case 'home':
    case 'الرئسية':
        require_once 'pages/home.view.php';
        break;
    case 'about':
        require_once 'pages/about.view.php';
        break;
    case 'login':
        require_once 'pages/login.view.php';
        break;
}

require_once 'app/template/footer.php';
