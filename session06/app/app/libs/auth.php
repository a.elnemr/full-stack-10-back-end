<?php

function createSession ($user)
{
    $_SESSION['login_user'] = $user;
    header('Location: index.php?p=home');
}


function checkUser()
{
    if (!checkIsUser()) {
        header('Location: index.php?p=login');
    }
}

function checkIsUser()
{
    if (empty($_SESSION['login_user'])) {
        return false;
    }

    return true;
}

//function checkIsUser()
//{
//    return empty($_SESSION['login_user'])? false : true;
//}