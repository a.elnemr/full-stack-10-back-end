<?php

// 0, n - 1
// numeric index array
/*$arr = array('Ahmed', 2, 3, 4);
$arr[] = 9;

print_r($arr);*/

// assoc array
//$student = ['Ahmed', 27, 'Giza'];
$student = ['name' => 'Ahmed', 'age' => 27, 'location' => 'Giza'];

// debugging
//print_r($student);
//var_dump($student);