<?php
//print_r($_GET);

$studentDegree = $_GET['degree'];

if (!is_numeric($studentDegree)) {
    echo 'Invalid Data should by number';
} else if ($studentDegree > 100) {
    echo 'Invalid Degree should be less than or equal 100';
} else if ($studentDegree >= 85) {
    echo 'A';
} else if ($studentDegree >= 75) {
    echo 'B';
} else if ($studentDegree >= 65) {
    echo 'C';
} else if ($studentDegree >= 55) {
    echo 'D';
} else {
    echo 'F';
}
?>
<p>
    <a href="home.php">Home</a>
</p>
