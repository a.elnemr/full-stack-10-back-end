<?php

namespace App\Http\Controllers;

use App\Todo;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $todo = Todo::all(['id', 'title', 'is_done']); // call model
        // resources/views/todo/index.blade.php
        return view('todo.index', compact('todo')); // bind data to view
    }

    public function store(Request $request)
    {
        /*$todo = new Todo();
        // get data from post form and assign to object attr title
        $todo->title = $request->get('title');
        $todo->save();*/
        Todo::create($request->all());
        return back();
    }

    public function destroy(Todo $todo)
    {
        // $todo = Todo::findOrFail($todo);
        $todo->delete();
        return redirect(route('todo.index'));
    }

    public function edit(Todo $todo)
    {
        return view('todo.index', [
            'targetItem' => $todo,
            'todo' => Todo::all(['id', 'title', 'is_done'])
        ]); // bind data to view
    }

    public function update(Request $request, Todo $todo)
    {
        $todo->update($request->all());
        return redirect(route('todo.index'));
    }

    public function create()
    {

    }
}
