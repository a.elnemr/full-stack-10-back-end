<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
//    protected $table = 'todos'; // optional

    protected $fillable = [
        'title',
        'is_done'
    ];
}
