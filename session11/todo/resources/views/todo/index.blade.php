<div class="container" style="width: 1220px; margin: auto">
    <h1>{{ isset($targetItem)? 'Update' : 'New' }} Todo</h1>
    <form action="{{ isset($targetItem)? route('todo.update', $targetItem) : route('todo.store') }}" method="post">
        @csrf
        @isset ($targetItem)
            @method('PUT')
        @endif
        <input type="text" name="title" value="{{ isset($targetItem)? $targetItem->title : '' }}">
        <input type="checkbox" name="is_done" value="1" {{ isset($targetItem) && $targetItem->is_done? 'checked' : '' }} >
        <button type="submit">{{ isset($targetItem)? 'Update' : 'Add' }}</button>
    </form>
    <hr>

    <ul>
        @foreach($todo as $item)
            <li>{{ $item->title }} | <a href="{{ route('todo.edit', $item) }}">Edit</a> |
                <form action="{{ route('todo.destroy', $item) }}"
                      method="post"
                      style="display: inline-block">
                    @method('DELETE')
                    @csrf
                    <button type="submit">Delete</button>
                </form>
            </li>

        @endforeach
    </ul>

</div>
