<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * CREATE TABLE todos(
            id BIGINT PRIMARY KEY AUTO_INCREMENT,
         *  title VARCHAR(255) NOT NULL,
         *  is_done tinyint DEFAULT '0' NOT NULL,
         *  created_at TIMESTAMP,
         *  updated_at TIMESTAMP
         * )
         */
        Schema::create('todos', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->boolean('is_done')->default(false);
            $table->timestamps();// created_at, updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todos');
    }
}
