#Laravel 7.* New Project
- To install laravel installer `composer global require laravel/installer` 
- `laravel new todo --auth` => `composer create-project --prefer-dist laravel/laravel todo`, and `composer req laravel/ui`
    - if we need install auth on existing project
    - `composer req laravel/ui`
    - `php artisan ui bootstrap --auth`
- `cd todo`
- `npm i`
- `npm run dev`
- create database from **phpmyadmin**
- edit **.env** update database connection
- open laravel server `php artisan serve`
- remove `database/migrations/create_fiaild_table.php`
    - `DDL -> Migrations`
    - `DML -> Model`
- `php artisan migrate`
    - if returned error 
    - update all index attrs and type `php artisan migrate:fresh` will drop all tables and re-create
    
- create list todo **route** `Route::get('/', 'TodoController@index');`
- create **view** `rescources/views/todo/index.blade.php`
- create **controller** `php artisan make:controller TodoController -r` -r ref to => https://laravel.com/docs/7.x/controllers#resource-controllers
- create table for todo list `php artisan make:migration create_todos_table`
- create **model** to connect between database and laravel project `php artisan make:model Todo`
- get all data in controller from `Todo` model in `index` function
## Resource Controller and Model and Migration
- `php artisan make:model Task -a`
