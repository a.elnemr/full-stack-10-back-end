<?php


namespace App\Http\Controllers;


use Symfony\Component\HttpFoundation\Request;

class PagesController extends Controller
{
    public function about(string $name)
    {
        return view('about', ['name' => $name, 'age' => 2]);// binding data
    }

    public function home()
    {
        return view('home');
    }

    public function save(Request $request)
    {
        // save data in database
        return redirect('/about-us/' . $request->get('name'));
    }
}
