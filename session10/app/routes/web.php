<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});


Route::get('/about-us/{name}', 'PagesController@about')->name('about');
Route::get('/home', 'PagesController@home');
Route::post('/v1/save', 'PagesController@save')->name('saveData');

