<?php

class Student extends User
{
    public $degree;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function setUser(User $user): ?string
    {
        return $user->name;
    }

    public function getPassword(): string
    {
        return md5($this->password);
    }

    public function __call($name, $arguments)
    {
        return 'from call function' . $arguments[0];
    }
}