DROP DATABASE lms2020;
CREATE DATABASE lms2020;

USE lms2020;
-- DROP TABLE users;
CREATE TABLE users (
  id INT PRIMARY KEY AUTO_INCREMENT,
  full_name VARCHAR (20) NOT NULL,
  email VARCHAR (20) NOT NULL UNIQUE, -- char(20)
  password VARCHAR (60) NOT NULL
);


-- DROP TABLE students;
CREATE TABLE students (
  id INT PRIMARY KEY AUTO_INCREMENT,
  full_name VARCHAR (20) NOT NULL,
  email VARCHAR (20) NOT NULL UNIQUE, -- char(20)
  phone CHAR (11) UNIQUE,
  gpa FLOAT NOT NULL DEFAULT '0',
  created_at TIMESTAMP
);

ALTER TABLE students ADD age INT NOT NULL DEFAULT '0';

TRUNCATE students;
ALTER TABLE `courses` ADD `instructor_name` VARCHAR(20) NULL AFTER `name`;

INSERT INTO `courses`(`name`, `instructor_name`, `duration`)
VALUES ('php', 'ahmed', '50')