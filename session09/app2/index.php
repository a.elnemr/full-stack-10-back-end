<?php
require_once __DIR__ . '/vendor/autoload.php';
require_once 'app/config/db_connection.php';
require_once 'app/libs/functions.php';
require_once 'app/model/User.class.php';
require_once 'app/model/Student.class.php';
require_once 'app/model/Teacher.class.php';
require_once 'app/model/Pages.class.php';


if (empty($_GET['p'])) {
    $page = 'index';
} else {

    $page = $_GET['p'];
}
$pageView = new Page;

$pageView->$page();