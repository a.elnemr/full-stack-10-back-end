<?php

class Page
{
    public $pageName;

    public function home()
    {
        require_once 'pages/home.view.php';
    }

    public function index()
    {
        require_once 'pages/index.view.php';
    }

    public function __call($name, $arguments)
    {
        require_once 'pages/404.view.php';
    }
}