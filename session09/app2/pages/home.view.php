<?php
global $dbh;
//// security
////    echo  $_GET['password'];
////    echo $dbh->quote($_GET['password']);
//$email = $dbh->quote($_GET['email']);
////    $email = $_GET['email'];
//$password = $dbh->quote($_GET['password']);
////    $password = $_GET['password'];
//
//// step 1 : write query in var
//$sql = "SELECT * FROM `users` WHERE `email` = :email AND `password` = :password";
//
//// step 2 : prepare query
//$stm = $dbh->prepare($sql);
//
//// step 3 : execute prepared query
//$stm->execute(['email' => $username, 'password' => $password]);
//
//// step 4 : optional in insert, update, delete, required in select
//$data = $stm->fetchAll(PDO::FETCH_ASSOC);
//
//print_r($data);

// step 1 : write query in var
$sql = 'SELECT * FROM `users`';

// step 2 : prepare query
$stm = $dbh->prepare($sql);

// step 3 : execute prepared query
$stm->execute();

// step 4 : optional in insert, update, delete, required in select
$data = $stm->fetchAll(PDO::FETCH_ASSOC);
//print_r($data);
?>

<table>
    <tr>
        <th>#</th>
        <th>name</th>
    </tr>
    <?php foreach ($data as $user): ?>
    <tr>
        <td><?= $user['id'] ?></td>
        <td><?= $user['full_name'] ?></td>
    </tr>
    <?php endforeach; ?>
</table>
