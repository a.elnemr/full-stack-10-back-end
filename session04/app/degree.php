<?php
//die;
$message = '';
$errors = [];

if (!empty($_POST['form']) && empty($_POST['student_name'])) {
    $errors[] = 'PLZ enter you name';
}
if (!empty($_POST['form']) && empty($_POST['degree'])) {
    $errors[] = 'PLZ enter you degree';
}
if (!empty($_POST['form']) && count($errors) === 0) {
    $degree = $_POST['degree'];

    if ($degree > 85) {
        $message = 'Welcome, ' . $_POST['student_name'] . ' your Score A';
    } else if ($degree > 75) {
        $message = 'Welcome, ' . $_POST['student_name'] . ' your Score B';
    } else if ($degree > 65) {
        $message = 'Welcome, ' . $_POST['student_name'] . ' your Score C';
    } else if ($degree > 50) {
        $message = 'Welcome, ' . $_POST['student_name'] . ' your Score D';
    } else {
        $message = 'Welcome, ' . $_POST['student_name'] . ' your Score F';
    }
}


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
<?php
if (count($errors)) :
    ?>
    <ul>
        <?php for ($i = 0; $i < count($errors); $i++) : ?>
            <li> <?= $errors[$i] ?> </li>
        <?php endfor; ?>
    </ul>
<?php
endif;
?>
<form method="post">
    Student Name:
    <input type="text" name="student_name" value="<?php
    if (!empty($_POST['student_name'])) {
        echo $_POST['student_name'];
    }
    ?>">
    <hr>
    Student Degree:
    <input type="number" name="degree" value="<?php
    if (!empty($_POST['degree'])) {
        echo $_POST['degree'];
    }
    ?>">
    <hr>
    <input type="submit" name="form">
</form>

<h1>
    <?= $message ?>
</h1>



</body>
</html>