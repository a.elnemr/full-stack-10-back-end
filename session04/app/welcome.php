<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <?php
    if (empty($_POST['username'])) :
        echo 'Please enter your username';
    else :
        ?>
        <h1>Welcome, <?= $_POST['username'] ?> </h1>
    <?php
    endif;
    ?>

</body>
</html>