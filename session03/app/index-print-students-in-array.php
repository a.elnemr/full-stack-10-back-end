<?php

$students = [
    ['id' => 1, 'name' => 'Mostafa', 'age' => 25],
    ['id' => 2, 'name' => 'Mohamed', 'age' => 22],
    ['id' => 3, 'name' => 'Ali', 'age' => 27],
    ['name' => 'sara', 'age' => 27, 'id' => 4],
    ['name' => 'Ahmed', 'age' => 27, 'id' => 5],
];

?>

<ul>
    <?php for ($i = 0; $i < count($students); $i++) : ?>
    <li>
        <?php /*echo $students[$i]['name'];*/ ?>
        <h3><?= $students[$i]['name'] ?></h3>
        <p><?= $students[$i]['age'] ?></p>
    </li>
    <?php endfor; ?>
</ul>