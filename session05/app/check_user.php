<?php

session_start();

if (empty($_SESSION['user'])) {
    exit(header('Location: login.php'));
}