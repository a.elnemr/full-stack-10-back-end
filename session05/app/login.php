<?php
session_start();

if (!empty($_SESSION['user'])) {
    header('Location: index.php');
    exit();
}

if (!empty($_POST['submit'])) {

    $_SESSION['user'] = [
            'id' => time(),
            'username' => $_POST['username']
    ];
    header('Location: index.php');
    exit();
}

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    Login Page
    <form method="post">
        username
        <input type="text" name="username">
        password
        <input type="password" name="password">
        <input type="submit" name="submit">
    </form>
</body>
</html>